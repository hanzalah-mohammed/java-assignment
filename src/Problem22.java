// String palindrome using StringBuilder
// ignoring the difference between uppercase and lowercase letters,
// the string "moM" is a palindrome.

import java.util.Scanner;

public class Problem22 {
    public static void main(String[] args) {

        Scanner refScanner = new Scanner(System.in);
        System.out.println("Enter a word:");
        String wordInput = refScanner.next().toLowerCase();
        String reverseWord = new StringBuilder(wordInput).reverse().toString().toLowerCase();

        if(reverseWord.equals(wordInput)){
            System.out.println(wordInput + " is a palindrome!");
        } else {
            System.out.println(wordInput + " is not a palindrome!");
        }
    }
}
